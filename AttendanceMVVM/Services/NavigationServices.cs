﻿using AttendanceMVVM.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AttendanceMVVM.Services
{
    public interface INavigationService
    {
        Core.ViewModel CurrentView { get; }
        void NavigateTo<TViewModel>() where TViewModel : Core.ViewModel;
    }
    public class NavigationServices : ObservableObject, INavigationService
    {
        private readonly Func<Type, Core.ViewModel> _viewModelFactory;
        public NavigationServices(Func<Type,Core.ViewModel> ViewModelFactory)
        { 
            _viewModelFactory = ViewModelFactory;
        }
        
        private Core.ViewModel _currentView;

        public Core.ViewModel CurrentView
        {
            get { return _currentView; }
            private set
            {
                _currentView = value;
                OnPropertyChanged();
            }
        }

        public void NavigateTo<TViewModel>() where TViewModel : Core.ViewModel
        {
            var viewmodel = _viewModelFactory.Invoke(typeof(TViewModel));
            CurrentView = viewmodel;
        }
    }
}
