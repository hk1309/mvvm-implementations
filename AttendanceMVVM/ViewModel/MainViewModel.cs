﻿using AttendanceMVVM.Core;
using AttendanceMVVM.Services;
using AttendanceMVVM.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttendanceMVVM.ViewModel
{
    public class MainViewModel : Core.ViewModel
    {
        private INavigationService _navigation;
        public INavigationService Navigation
        {
            get { return _navigation; }
            set { 
                _navigation = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand NavigateEmployeeLoginCommand { get; set; }
        public RelayCommand NavigateAdminLoginCommand { get; set; }
        public MainViewModel(INavigationService navService) 
        {
            Navigation = navService;
            NavigateEmployeeLoginCommand = new RelayCommand(execute: o => { Navigation.NavigateTo<EmployeeLoginViewModel>(); } , canExecute: o => true );
            NavigateAdminLoginCommand = new RelayCommand(execute: o => { Navigation.NavigateTo<AdminLoginViewModel>(); }, canExecute: o => true);

        }

    }
}
