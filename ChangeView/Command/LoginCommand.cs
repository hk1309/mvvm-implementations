﻿using ChangeView.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Configuration;
using System.Data;
using System.Windows.Controls;
using System.Runtime.Remoting.Messaging;
using System.Windows;

namespace ChangeView.Command
{
    public class LoginCommand : ICommand
    {       
        //string strConnection = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
        private EmployeeLoginViewModel EmpLoginVM;

        public LoginCommand(EmployeeLoginViewModel EmpLoginVM)
        {
            this.EmpLoginVM = EmpLoginVM;
        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=SP13882\\SQLEXPRESS;Initial Catalog=AttendanceApp;Integrated Security=True");
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                string searchQuery = "SELECT FirstName, LastName, EmpID FROM EmployeeDetails WHERE EmpUsername = '" + EmpLoginVM.UserName + "' AND EmpPassword = '" + EmpLoginVM.UserPassword + "'";
                SqlCommand Command = new SqlCommand(searchQuery, conn);

                SqlDataReader reader = Command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        MessageBox.Show("Login SuccessFull");
                        //EmpLoginVM.IsLoggedIn = true;
                    }                    
                }
                else
                {
                    MessageBox.Show("Login UnsuccessFull");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
