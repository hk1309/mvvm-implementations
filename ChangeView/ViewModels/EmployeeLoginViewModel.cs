﻿using ChangeView.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ChangeView.ViewModels
{
    public class EmployeeLoginViewModel : BaseViewModel
    {

        private string _userName;
        private string _userPassword;

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged(nameof(UserName));
            }
        }

        public string UserPassword
        {
            get { return _userPassword; }
            set
            {
                _userPassword = value;
                OnPropertyChanged(nameof(UserPassword));
            }
        }

        public ICommand LoginCommand { get; set; }

        public EmployeeLoginViewModel()
        {
            LoginCommand = new LoginCommand(this);
        }
    }
}
