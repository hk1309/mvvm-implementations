﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CommandDemo
{
    public class ViewModel
    {
        //instande of ICommand Interface
        public ICommand MyCommand { get; set; }



        public ViewModel() 
        {
            //pointing the instance of ICommand Interface to the Command class
            MyCommand = new Command(ExecuteMethod,CanExecuteMethod);
        
        }

        private bool CanExecuteMethod(object parameter)
        {
            return true;
        }

        private void ExecuteMethod(object parameter) 
        {
            MessageBox.Show("I am a button without code behind file");
        }
    }


}
