﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DataGrid
{
    internal class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        
        public MainViewModel() 
        {
            Person = new ObservableCollection<Person>()
            {
                new Person() { Fname = "hemant", Lname = "Kumar" },
                new Person() { Fname = "abhinav", Lname = "chahal" },
                new Person() { Fname = "abhinav", Lname = "sharma" }
            };

            //Person2 = new ObservableCollection<Person>();
            

            

        }

        private ICommand _command;
        public ICommand Command
        {
            get {
                    if (_command == null)
                    {
                        _command = new Command(PerformCommand, CanPerform);
                    }
                    return _command; 
            }
            set
            {
                _command = value;
            }
        }

        private int _column;
        public int Column
        {
            get { return _column; }
            set
            {
                _column = value;
                OnPropertyChanged(nameof(Column));
            }
        }

        private bool _visibility;
        public bool Visibility
        {
            get { return _visibility; }
            set
            {
                _visibility = value;
                OnPropertyChanged(nameof(Visibility));
            }
        }

        private bool _nonEditable;
        public bool NonEditable
        {
            get { return _nonEditable; }
            set
            {
                _nonEditable = value;
                OnPropertyChanged(nameof(NonEditable));
            }
        }


        private ObservableCollection<Person> _person;
        public ObservableCollection<Person> Person
        {
            get { return _person; }
            set
            {
                _person = value;
                //OnPropertyChanged(nameof(Person));
            }
        }

        //private ObservableCollection<Person> _person2;
        //public ObservableCollection<Person> Person2
        //{
        //    get { return _person2; }
        //    set
        //    {
        //        _person2 = value;
        //        //OnPropertyChanged(nameof(Person));
        //    }
        //}
        private bool CanPerform(object parameter)
        {
            return true;
        }

        private void PerformCommand(object parameter)
        {
            if (parameter.ToString() == "Left")
            {
                Column = 0;
                Visibility = true;
                NonEditable = false;
            }
            else if (parameter.ToString() == "Right")
            {
                Column = 2;
                Visibility = true;
                NonEditable = true;
            }
        }
    }
}
