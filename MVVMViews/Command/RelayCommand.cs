﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVMViews.Command
{
    public class RelayCommand : ICommand
    {
        Action<Object> ExecuteAction;
        Func<object, bool> canExecute;

        public RelayCommand(Action<Object> ExecuteAction, Func<object, bool> canExecute)
        {
            this.ExecuteAction = ExecuteAction;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (canExecute == null)
            {
                return true;
            }
            else
            {
                return canExecute(parameter);
            }
        }
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
        public void Execute(object parameter)
        {
            ExecuteAction(parameter);
        }
    }
}
