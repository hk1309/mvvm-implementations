﻿using System.Threading;
namespace PassingParameterToThreadFunction
{
    //declaring a callback delegate
    public delegate void SumOfNumberCallbackDelegate(int SumOfNumber);
    class Program
    {
     
        //callbaclk method.....writing a callback method whose signature matches the signature of the callback delegate
        public static void DisplaySumOfNumber(int Sum)
        {
            Console.WriteLine("The sum of numbers is: "+Sum); 
        }
        public static void Main(string[] args)
        {
            int max = 10;
            //initializing the delegate....... This delegate will accept the method with the same signature  
            SumOfNumberCallbackDelegate _callback = new SumOfNumberCallbackDelegate(DisplaySumOfNumber);
            NumberHelperClass _helper = new NumberHelperClass(max , _callback);
            ThreadStart obj = new ThreadStart(_helper.CalculateSum );
            Thread thread = new Thread(obj);
            thread.Start();
            Console.ReadLine();
        }

    }

    class NumberHelperClass 
    {
        private int _number;
        SumOfNumberCallbackDelegate _callbackDelegate;
        public NumberHelperClass(int number, SumOfNumberCallbackDelegate callbackDelegate)
        {
            _number = number;
            _callbackDelegate = callbackDelegate;
        }

        public void CalculateSum()
        {
            int sum = 0;
            for (int i = 0 ; i < _number; i++)
            {
                sum += i;
            }


            //passing value to the callback delegate
            if(_callbackDelegate!= null)
            {
                _callbackDelegate(sum); 
            }
        }
    }
}
