﻿using System.Threading;
namespace ThreadLocking
{
    class Program
    {
        public void Display()
        {
            //it restricts the access of another thread untill the executing thread releases the lock, then this block is available for other thread
            lock (this)
            {
                Console.Write("Hello, my name is ");
                Thread.Sleep(5000);
                Console.WriteLine("Hemant Kumar");
            }
        }
        static void Main(string[] args)
        {
            Program obj = new Program();
            Thread t1 = new Thread(obj.Display);
            Thread t2 = new Thread(obj.Display);
            Thread t3 = new Thread(obj.Display);
            t1.Start();
            t2.Start();
            t3.Start();
            Console.ReadKey();
        }
    }
}